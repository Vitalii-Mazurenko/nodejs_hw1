const fs = require('fs');
const path = require('path');
const filesFolder = './files/';
function isFilenameCorrect(filename) {
  const extensions = [ 'log', 'txt', 'json', 'yaml', 'xml', 'js'];

  const splitFilename = filename.split('.');
  if ( !splitFilename.length ) return false;

  const fileExtension = splitFilename[splitFilename.length - 1];
  return extensions.includes(fileExtension);
}

function createFile (req, res, next) {
  const { filename, content } = req.body;
  if( filename === undefined || content === undefined ) {
    next({message: 'Filename or content is not provided', status: 400})
    return;
  }

  if( !fs.existsSync(filesFolder) ) {
    fs.mkdirSync('files');
  }

  if( !isFilenameCorrect( filename) ) {
    next({message: 'Filename extension in not correct', status: 400})
    return;
  }

  const filePath = filesFolder + filename;

  if( fs.existsSync(filePath) ) {
    next({message: `There is already file with name: ${filename}`, status: 400})
    return;
  }

  fs.writeFile( filePath, JSON.stringify(content), (err) => {
    if( err ) {
      next({message: 'Failed to create file', status: 500})
      return;
    }
    res.status(200).send({ "message": "File created successfully" });
  })
}

function getFiles (req, res, next) {

  fs.readdir(filesFolder, (err, files) => {
    if( err ) {
      next({message: 'Failed to read folder', status: 500})
      return;
    }
    res.status(200).send({
      "message": "Success",
      "files": files});
  });
}

const getFile = (req, res, next) => {
  const { filename } = req.params;
  const filePath = filesFolder + filename;

  if( !fs.existsSync(filePath) ) {
    next({message: 'There is no file with such name', status: 400})
    return;
  }

  const data = fs.readFileSync( filePath, {encoding:'utf-8', flag: 'r'});
  const {birthtime: uploadedDate} = fs.statSync(filePath);


  const response = {
    message: 'Success',
    filename,
    content: JSON.parse(data),
    extension: path.extname(filename).replace('.', ''),
    uploadedDate
  };

  res.status(200).send(response);
}

// Other functions

const editFile = (req, res, next) => {
  const { filename, content } = req.body;

  if( filename === undefined || content === undefined ) {
    next({message: 'Filename of content is not provided', status: 400})
    return;
  }

  const filePath = filesFolder + filename;

  if( !fs.existsSync(filePath) ) {
    next({message: 'There is no file with such name', status: 400})
    return;
  }

  fs.writeFile( filePath, content,{encoding: 'utf-8', flag: 'a'}, (err) => {
    res.status(200).send({ "message": `File: ${filename} was successfully edited` });
  })

}

const deleteFile = (req, res, next) => {
  const {filename} = req.params;
  const filePath = filesFolder + filename;
  if( !fs.existsSync(filePath) ) {
    next({message: 'There is no file with such name', status: 400})
    return;
  }
  fs.unlinkSync(filePath);

  res.status(200).send({'message': `File ${filename} was successfully deleted`})
}

// path.extName('file.txt') ---> '.txt'

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
